
#include <iostream>
#include "g/g.h"
#include "d/d.h"

namespace g {
  void addIndentation(uint indentation) {
    for (uint i = 0; i < indentation; ++i) {
      std::cout << "  ";
    }
  }

  void doThingG(uint indentation) {
    addIndentation(indentation);
    std::cout << "G v1" << std::endl;

    d::doThingD(indentation + 1);
  };
}
